﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using contacts.Models;
using contacts.ViewModels;
using AutoMapper;
using System.Data.SqlClient;

namespace contacts.Controllers
{
    public class ContactsController : ApiController
    {
        private ContactEntities db = new ContactEntities();

        // GET: api/Contacts
        public IEnumerable<ContactViewModel> GetContacts()
        {
            //return db.Contacts;
            IEnumerable<Contact> contacts;
            IEnumerable<ContactViewModel> viewModelContacts;

            contacts = db.Contacts.ToList();

            viewModelContacts = Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactViewModel>>(contacts);
            return viewModelContacts;
        }

        [Route("~/api/searchcontacts")]
        public IEnumerable<ContactViewModel> GetContacts(string firstname, string lastname, string title)
        {
            //return db.Contacts;
            //IEnumerable<Contact> contacts;
            IEnumerable<ContactViewModel> viewModelContacts;

            viewModelContacts = db.Database.SqlQuery<ContactViewModel>("EXEC uspSearchContacts " + (firstname != null ? firstname : "NULL") + ", " + (lastname != null ? lastname : "NULL") + ", " + (title != null ? title : "NULL") + ";").ToList();

            //viewModelContacts = Mapper.Map<IEnumerable<Contact>, IEnumerable<ContactViewModel>>(contacts);
            return viewModelContacts;
        }

        // GET: api/Contacts/5
        [ResponseType(typeof(Contact))]
        public IHttpActionResult GetContact(int id)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return NotFound();
            }

            ContactViewModel contactViewModel = Mapper.Map<Contact, ContactViewModel>(contact);

            return Ok(contactViewModel);
        }

        // PUT: api/Contacts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutContact(int id, Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != contact.ContactID)
            {
                return BadRequest();
            }

            db.Entry(contact).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Contacts
        [ResponseType(typeof(Contact))]
        public IHttpActionResult PostContact(ContactFormViewModel contactFormViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Contact contact = Mapper.Map<ContactFormViewModel, Contact>(contactFormViewModel);

            db.Contacts.Add(contact);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = contact.ContactID }, contact);
        }

        // DELETE: api/Contacts/5
        [ResponseType(typeof(Contact))]
        public IHttpActionResult DeleteContact(int id)
        {
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return NotFound();
            }

            db.Contacts.Remove(contact);
            db.SaveChanges();

            return Ok(contact);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ContactExists(int id)
        {
            return db.Contacts.Count(e => e.ContactID == id) > 0;
        }
    }
}