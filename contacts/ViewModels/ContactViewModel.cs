﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.ViewModels
{
    public class ContactViewModel
    {
        public int ContactID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime? DateCreated { get; set; }

        public List<PhoneViewModel> Phones { get; set; }
        public List<EmailViewModel> Emails { get; set; }
        public List<TagViewModel> Tags { get; set; }
    }
}