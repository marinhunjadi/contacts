﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.ViewModels
{
    public class EmailViewModel
    {
        public int EmailID { get; set; }
        public string EmailAddress { get; set; }

        public int ContactID { get; set; }
    }
}