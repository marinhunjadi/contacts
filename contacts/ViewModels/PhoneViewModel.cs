﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.ViewModels
{
    public class PhoneViewModel
    {
        public int PhoneID { get; set; }
        public string Number { get; set; }

        public int ContactID { get; set; }
    }
}