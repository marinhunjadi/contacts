﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.ViewModels
{
    public class TagViewModel
    {
        public int TagID { get; set; }
        public string Title { get; set; }

        public int ContactID { get; set; }
    }
}