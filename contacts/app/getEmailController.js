﻿app.controller("getEmailCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/emails/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.EmailID = $scope.myData.EmailID;
        $scope.EmailAddress = $scope.myData.EmailAddress;
        $scope.ContactID = $scope.myData.ContactID;
        $scope.deleteData = function () {
            $http({
                method: 'DELETE',
                url: 'api/emails/' + $routeParams.id,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                alert("Success!");
                window.history.back();
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
});