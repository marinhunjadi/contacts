﻿app.controller("postPhoneCtrl", function ($scope, $http, $routeParams) {
    $scope.Number = "";
    $scope.ContactID = $routeParams.contact;

    $scope.submitData = function () {
        var data = {
            Number: $scope.Number,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'POST',
            url: 'api/phones',
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});