﻿app.controller("putEmailCtrl", function ($scope, $http, $routeParams) {
    $scope.EmailID = "";//$routeParams.id;
    $scope.EmailAddress = "";
    $scope.ContactID = "";//$routeParams.contact;

    $http.get("/api/emails/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.EmailID = $scope.myData.EmailID;
        $scope.EmailAddress = $scope.myData.EmailAddress;
        $scope.ContactID = $scope.myData.ContactID;
    }, function (response) {
        alert(response.statusText);
    });

    $scope.submitData = function () {
        var data = {
            EmailID: $scope.EmailID,
            EmailAddress: $scope.EmailAddress,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'PUT',
            url: '/api/emails/' + $routeParams.id,
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});