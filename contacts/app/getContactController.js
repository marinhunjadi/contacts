﻿app.controller("getContactCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/contacts/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.FirstName = $scope.myData.FirstName;
        $scope.LastName = $scope.myData.LastName;
        $scope.Address = $scope.myData.Address;
        $scope.ContactID = $scope.myData.ContactID;
        $scope.submitData = function () {
            var data = {
                ContactID: $scope.ContactID,
                FirstName: $scope.FirstName,
                LastName: $scope.LastName,
                Address: $scope.Address
            }
            $http({
                method: 'PUT',
                url: '/api/contacts/' + $routeParams.id,
                data: JSON.stringify(data),
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                alert("Success!");
            }, function (response) {
                alert(response.statusText);
            });
        };
        $scope.deleteData = function () {
            $http({
                method: 'DELETE',
                url: 'api/contacts/' + $routeParams.id,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                alert("Success!");
                window.history.back();
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
    $http.get("/api/contacts/" + $routeParams.id + "/tags").then(function (response) {
        $scope.myData2 = response.data;
    }, function (response) {
        alert(response.statusText);
    });
});