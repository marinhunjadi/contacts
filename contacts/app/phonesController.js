﻿app.controller("phonesCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/contacts/" + $routeParams.contact + "/phones").then(function (response) {
        $scope.myData2 = response.data;
        $scope.contactID = $routeParams.contact;
    }, function (response) {
        alert(response.statusText);
    });
});