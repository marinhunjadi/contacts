﻿app.controller("putTagCtrl", function ($scope, $http, $routeParams) {
    $scope.TagID = "";//$routeParams.id;
    $scope.Title = "";
    $scope.ContactID = "";//$routeParams.contact;

    $http.get("/api/tags/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.TagID = $scope.myData.TagID;
        $scope.Title = $scope.myData.Title;
        $scope.ContactID = $scope.myData.ContactID;
    }, function (response) {
        alert(response.statusText);
    });

    $scope.submitData = function () {
        var data = {
            TagID: $scope.TagID,
            Title: $scope.Title,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'PUT',
            url: '/api/tags/' + $routeParams.id,
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});