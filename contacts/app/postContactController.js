﻿app.controller("postContactCtrl", function ($scope, $http) {
    $scope.FirstName = "";
    $scope.LastName = "";
    $scope.Address = "";
    $scope.submitData = function () {
        var data = {
            FirstName: $scope.FirstName,
            LastName: $scope.LastName,
            Address: $scope.Address
        }
        $http({
            method: 'POST',
            url: 'api/contacts',
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        //$http.post('/api/contacts/post', JSON.stringify(data))
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});