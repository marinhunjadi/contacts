﻿app.controller("getPhoneCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/phones/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.PhoneID = $scope.myData.PhoneID;
        $scope.Number = $scope.myData.Number;
        $scope.ContactID = $scope.myData.ContactID;
        $scope.deleteData = function () {
            $http({
                method: 'DELETE',
                url: 'api/phones/' + $routeParams.id,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                alert("Success!");
                window.history.back();
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
});