﻿app.controller("putPhoneCtrl", function ($scope, $http, $routeParams) {
    $scope.PhoneID = "";//$routeParams.id;
    $scope.Number = "";
    $scope.ContactID = "";//$routeParams.contact;

    $http.get("/api/phones/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.PhoneID = $scope.myData.PhoneID;
        $scope.Number = $scope.myData.Number;
        $scope.ContactID = $scope.myData.ContactID;
    }, function (response) {
        alert(response.statusText);
    });

    $scope.submitData = function () {
        var data = {
            PhoneID: $scope.PhoneID, 
            Number: $scope.Number,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'PUT',
            url: '/api/phones/' + $routeParams.id,
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});