﻿app.controller("contactsCtrl", function ($scope, $http) {
    $scope.FirstName = "";
    $scope.LastName = "";
    $scope.Tag = "";
    $http.get("/api/contacts").then(function (response) {
        $scope.myData = response.data;
        $scope.submitData = function () {            
            $http.get("/api/searchcontacts/?firstname=" + $scope.FirstName + "&lastname=" + $scope.LastName + "&title=" + $scope.Tag)
            .then(function (response) {
                $scope.myData = response.data;
            }, function (response) {
                alert(response.statusText);
            });
        };
        $scope.resetData = function () {
            $http.get("/api/contacts")
            .then(function (response) {
                $scope.myData = response.data;
                $scope.FirstName = "";
                $scope.LastName = "";
                $scope.Tag = "";
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
});