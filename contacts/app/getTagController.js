﻿app.controller("getTagCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/tags/" + $routeParams.id).then(function (response) {
        $scope.myData = response.data;
        $scope.TagID = $scope.myData.TagID;
        $scope.Title = $scope.myData.Title;
        $scope.ContactID = $scope.myData.ContactID;
        $scope.deleteData = function () {
            $http({
                method: 'DELETE',
                url: 'api/tags/' + $routeParams.id,
                headers: { 'Content-Type': 'application/json' }
            })
            .then(function (response) {
                alert("Success!");
                window.history.back();
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
});