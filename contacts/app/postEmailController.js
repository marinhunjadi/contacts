﻿app.controller("postEmailCtrl", function ($scope, $http, $routeParams) {
    $scope.EmailAddress = "";
    $scope.ContactID = $routeParams.contact;
    $scope.submitData = function () {
        var data = {
            EmailAddress: $scope.EmailAddress,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'POST',
            url: 'api/emails',
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});