﻿app.controller("postTagCtrl", function ($scope, $http, $routeParams) {
    $scope.Title = "";
    $scope.ContactID = $routeParams.contact;
    $scope.submitData = function () {
        var data = {
            Title: $scope.Title,
            ContactID: $scope.ContactID
        }
        $http({
            method: 'POST',
            url: 'api/tags',
            data: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(function (response) {
            alert("Success!");
        }, function (response) {
            alert(response.statusText);
        });
    };
});