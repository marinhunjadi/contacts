﻿app.controller("putContactCtrl", function ($scope, $http, $routeParams) {
    /*$scope.FirstName = "";
    $scope.LastName = "";
    $scope.Address = "";
    $scope.ContactID = "0";*/
    $http.get("/api/contacts/" + $routeParams.id).then(function (response) {
        //alert("OK");
        $scope.myData = response.data;
        $scope.FirstName = $scope.myData.FirstName;
        $scope.LastName = $scope.myData.LastName;
        $scope.Address = $scope.myData.Address;
        $scope.ContactID = $scope.myData.ContactID;
        $scope.submitData = function () {
            var data = {
                ContactID: $scope.ContactID,
                FirstName: $scope.FirstName,
                LastName: $scope.LastName,
                Address: $scope.Address
            }
            //alert(jQuery('#myForm').serialize());
            $http({
                method: 'PUT',
                url: '/api/contacts/' + $routeParams.id,
                data: JSON.stringify(data), //jQuery('#myForm').serialize(),
                headers: { 'Content-Type': 'application/json' }
            })
            //$http.post('/api/contacts/post', jQuery('#myForm').serialize())
            .then(function (response) {
                alert("Success!");
            }, function (response) {
                alert(response.statusText);
            });
        };
    }, function (response) {
        alert(response.statusText);
    });
});