﻿app.controller("emailsCtrl", function ($scope, $http, $routeParams) {
    $http.get("/api/contacts/" + $routeParams.contact + "/emails").then(function (response) {
        $scope.myData2 = response.data;
        $scope.contactID = $routeParams.contact;
    }, function (response) {
        alert(response.statusText);
    });
});