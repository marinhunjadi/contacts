﻿using AutoMapper;
using contacts.Models;
using contacts.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Contact, ContactViewModel>();
                cfg.CreateMap<Phone, PhoneViewModel>();
                cfg.CreateMap<Email, EmailViewModel>();
                cfg.CreateMap<Tag, TagViewModel>();
                cfg.CreateMap<ContactFormViewModel, Contact>()
                    //.ForMember(c => c.ContactID, map => map.MapFrom(vm => vm.ContactID))
                    .ForMember(c => c.FirstName, map => map.MapFrom(vm => vm.FirstName))
                    .ForMember(c => c.LastName, map => map.MapFrom(vm => vm.LastName))
                    .ForMember(c => c.Address, map => map.MapFrom(vm => vm.Address));
                    //.ForMember(c => c.DateCreated, map => map.MapFrom(vm => vm.DateCreated));
            });
        }
    }
}