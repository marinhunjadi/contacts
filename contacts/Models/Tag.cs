﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.Models
{
    public class Tag
    {
        public int TagID { get; set; }
        public string Title { get; set; }

        public int ContactID { get; set; }
        public Contact Contact { get; set; }
    }
}