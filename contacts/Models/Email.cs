﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.Models
{
    public class Email
    {
        public int EmailID { get; set; }
        public string EmailAddress { get; set; }

        public int ContactID { get; set; }
        public Contact Contact { get; set; }
    }
}