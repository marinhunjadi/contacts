﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace contacts.Models
{
    public class ContactSeedData : DropCreateDatabaseIfModelChanges<ContactEntities>
    {
        protected override void Seed(ContactEntities context)
        {
            GetContacts().ForEach(c => context.Contacts.Add(c));
            GetPhones().ForEach(p => context.Phones.Add(p));
            GetEmails().ForEach(e => context.Emails.Add(e));
            context.Commit();
            string query = @"
                CREATE PROCEDURE uspSearchContacts
                    @FirstName varchar(255) = NULL,
                    @LastName varchar(255) = NULL,
                    @Title varchar(255) = NULL
                AS
                    SET NOCOUNT ON;
                    SELECT c.* FROM Contacts c INNER JOIN Tags t on c.ContactID = t.ContactID
                    WHERE (FirstName = @FirstName OR @FirstName IS NULL) AND (LastName = @LastName OR @LastName IS NULL) AND (Title = @Title OR @Title IS NULL);";
            context.Database.ExecuteSqlCommand(query);
        }

        private static List<Contact> GetContacts()
        {
            return new List<Contact>
            {
                new Contact {
                    FirstName = "A",
                    LastName = "B",
                    Address = "C"
                },
                new Contact {
                    FirstName = "A",
                    LastName = "B",
                    Address = "C"
                },
                new Contact {
                    FirstName = "A",
                    LastName = "B",
                    Address = "C"
                }
            };
        }

        private static List<Phone> GetPhones()
        {
            return new List<Phone>
            {
                new Phone {
                    Number = "123456",                    
                    ContactID = 1
                },
                new Phone {
                    Number = "123456",
                    ContactID = 2
                },
                new Phone {
                    Number = "123456",
                    ContactID = 3
                }
            };
        }

        private static List<Email> GetEmails()
        {
            return new List<Email>
            {
                new Email {
                    EmailAddress = "test@test.com",
                    ContactID = 1
                },
                new Email {
                    EmailAddress = "test@test.com",
                    ContactID = 2
                },
                new Email {
                    EmailAddress = "test@test.com",
                    ContactID = 3
                }
            };
        }
    }
}