﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.Models
{
    public class Contact
    {
        public int ContactID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public DateTime? DateCreated { get; set; }

        public virtual List<Phone> Phones { get; set; }
        public virtual List<Email> Emails { get; set; }
        public virtual List<Tag> Tags { get; set; }

        public Contact()
        {
            DateCreated = DateTime.Now;
        }
    }
}