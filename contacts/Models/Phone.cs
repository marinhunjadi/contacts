﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace contacts.Models
{
    public class Phone
    {
        public int PhoneID { get; set; }
        public string Number { get; set; }

        public int ContactID { get; set; }
        public Contact Contact { get; set; }
    }
}